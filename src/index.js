import React from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom";

import * as serviceWorker from "./serviceWorker";
import "./style.css";
import App from "./App";

render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("root")
);

serviceWorker.register();
