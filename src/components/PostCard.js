import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import MoreVertIcon from "@material-ui/icons/MoreVert";

import { truncate } from "../utils";

const useStyles = makeStyles(theme => ({
  card: {
    width: "100%"
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  }
}));

function createMarkup(data) {
  return { __html: truncate(data, 12) };
}

export default function PostCard({ post }) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={<Avatar src={post.author.avatar_URL} alt={post.author.name} />}
        action={
          <IconButton aria-label="settings">
            <MoreVertIcon />
          </IconButton>
        }
        title={<div dangerouslySetInnerHTML={createMarkup(post.title)} />}
        subheader="September 14, 2016"
      />
      <CardMedia
        className={classes.media}
        image={post.post_thumbnail ? post.post_thumbnail.URL : null}
        title="Paella dish"
      />
      <CardContent>
        <Typography
          variant="body2"
          color="textSecondary"
          component="p"
          dangerouslySetInnerHTML={createMarkup(post.excerpt)}
        />
      </CardContent>
    </Card>
  );
}
