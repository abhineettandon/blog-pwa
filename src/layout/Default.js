import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";

import Header from "./Header";

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(4)
  }
}));

function DefaultLayout({ children }) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Header />
      <Container className={classes.container}>{children}</Container>
    </React.Fragment>
  );
}

export default DefaultLayout;
