import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";

function Header() {
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6">Posts</Typography>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
