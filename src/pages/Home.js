import React from "react";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

import PostCard from "../components/PostCard";

function Home() {
  const [posts, setPosts] = React.useState([]);

  React.useEffect(() => {
    axios
      .get(
        "https://public-api.wordpress.com/rest/v1.1/sites/en.blog.wordpress.com/posts"
      )
      .then(({ data }) => setPosts(data.posts))
      .catch(error => console.log(error));
  }, []);

  return (
    <React.Fragment>
      <Grid container spacing={2}>
        {posts.map(post => (
          <Grid item md={4} key={post.ID} style={{ width: "100%" }}>
            <PostCard post={post} />
          </Grid>
        ))}
      </Grid>
    </React.Fragment>
  );
}

export default Home;
