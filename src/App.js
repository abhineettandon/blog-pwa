import React from "react";
import { Switch, Route } from "react-router-dom";

import DefaultLayout from "./layout/Default";
import Home from "./pages/Home";

function Routes() {
  return (
    <Switch>
      <Route path="/" component={Home} />
    </Switch>
  );
}

function App() {
  return (
    <DefaultLayout>
      <Routes />
    </DefaultLayout>
  );
}

export default App;
